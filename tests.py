# -*- encoding:iso8859-1 -*-
import unittest
import datetime
import random
import copy

from decimal import Decimal
from datetime import date

from banking.statements.reader import StatementAnalyzer
from banking.statements.plugin import DummyReaderPlugin

from banking.statements import AccountEntryRecord

# random references and statements for testing purposes
randomref = lambda: "%i-%i" % (random.randint(10000,99999), random.randint(10000,99999))
randomrefs = [randomref for i in range(50)] + ["00000-00000"] * 50


def randomentries():

   expense = AccountEntryRecord(
      datetime.date.today(),
      Decimal('-50'),
      random.sample(("Pekka Saaja","Janne Rahansaaja",u"Essi Myyjä"),1),
      "PAYMENT TO",
      "",
      "123456-123456",
      "dummy payment to someone"
   )

   income = AccountEntryRecord(
      datetime.date.today(),
      Decimal('50'),
      random.sample(("Pelle Maksaja","Jesse Rahanmeno", "Essi Ostaja"),1),
      "PAYMENT FROM",
      "100100102",
      "123456-123457",
      "dummy payment from someone"
   )

   entries = list(50 * (income,) + 50 * (expense,))
   random.shuffle(entries)
   refs = copy.copy(randomrefs)
   for e in entries:
      e._replace(reference=refs.pop())
   return entries


class StatementReadingTest(unittest.TestCase):

   def setUp(self):
     self.reader = StatementAnalyzer()
     self.reader += DummyReaderPlugin(randomentries())

   def testReading(self):
      for e in self.reader:
         assert type(e[0]) == datetime.date
         assert type(e[-1]) in (str, unicode)

   def testYearFiltering(self):
      for e in self.reader.entries(year=1970):
         assert(e.date.year != 1970)

   def testYearFiltering2(self):
      for e in self.reader.entries(year=datetime.date.today().year):
         assert(e.date.year == datetime.date.today().year)

   def testMonthFiltering(self):
      for e in self.reader.entries(month=2):
         assert(e.date.month == 2)

   def testMonthAndYearFiltering(self):
      for e in self.reader.entries(month=2, year=datetime.date.today()):
         assert(e.date.year == datetime.date.today())
         assert(e.date.month == 2)

   def testIncomeFiltering(self):
      for e in self.reader.income():
         assert(e.amount.is_signed() == False)
         break

   def testExpenseFiltering(self):
      for e in self.reader.expense():
         assert(e.amount.is_signed() == True)
         break

   def testCurrentYearAndMonthFiltering(self):
      for e in self.reader.year:
         assert(e.date.year == datetime.date.today().year)
         break
      for e in self.reader.month:
         assert(e.date.month == datetime.date.today().month)
         break

   def testReferenceFiltering(self):
      "all have reference?"
      for e in self.reader.referenced(None):
         assert(e.reference)
      "reference is one of those generated?"
      for e in self.reader.referenced(randomrefs):
         assert(e.reference in randomrefs)

   def testLength(self):
      assert len(self.reader) == len([e for e in randomentries()])


test_suite = unittest.makeSuite(StatementReadingTest)

if __name__ == "__main__":
   unittest.main()
