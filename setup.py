from setuptools import setup, find_packages
import os

version = '1.5.4'

setup(name='banking.statements',
      version=version,
      description="Reading and manipulation of bank statements",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Programming Language :: Python",
        ],
      keywords='',
      author='Petri Savolainen',
      author_email='petri.savolainen@koodaamo.fi',
      url='http://www.koodaamo.fi',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['banking','banking.statements'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      scripts= [
         'scripts/process_statements',
      ],
      test_suite = "tests.test_suite",
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
