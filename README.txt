============
Introduction
============

The StatementAnalyzer is a convenience class. Import the plugins you need:

>>> from banking.statements.plugins import MyBankReaderPlugin as Plugin

Then load the file to it:

>>> p = Plugin(fileobj)

Finally, load it into the analyzer

a = StatementAnalyzer()
a += p

So you can do:

>>> for entry in analyzer:
>>>   "do whatever here"

>>> len(analyzer)

>>> for e in analyzer.expenses:
       "count totals or something"

Likewise for analyzer.income. Etc. etc.

Plugins
-------

Banks have lots of different CSV formats for downloading entry data.
They change sometimes, without notice. Each plugin is responsible
for parsing the CSV formats of a bank to a common format.
