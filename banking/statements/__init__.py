# See http://peak.telecommunity.com/DevCenter/setuptools#namespace-packages
try:
    __import__('pkg_resources').declare_namespace(__name__)
except ImportError:
    from pkgutil import extend_path
    __path__ = extend_path(__path__, __name__)


from util import prettyprint, get_plugin, get_all_plugins
from reader import StatementAnalyzer

from collections import namedtuple

AccountEntryRecord = namedtuple ("AccountEntryRecord",
   ("date", "amount", "payee_or_recipient", "description", "account", "reference", "message")
)
