# -*- encoding: utf-8 -*-
from contextlib import contextmanager
import sys
import string
import codecs
import csv
import datetime
from decimal import Decimal

from util import prettyprint


class StatementAnalyzer:
   """A bank statement analyzer that can be queried for
   various kinds of information.
   """

   def __init__(self, debug=False):
      "initiate a reader, given a source (plugin object)"

      self._debug = debug
      self._cache = []
      self._length = 0


   def __iter__(self):
      "iterate over all entries being analyzed"
      for e in self._cache:
         yield e


   def __iadd__(self, source):
      "load statement entries from sources (bank plugins)"
      for entry in source:
         self._cache.append(entry)
      return self


   def __len__(self):
      return len(self._cache)


   def entries(self, year=None, month=None):
      "iterate over entries, optionally restricted by year, month or both"

      # try reading from source first, optionally caching what's read

      if year and month:
         return (e for e in self if e.date.year == year and e.date.month == month)
      elif year:
         return (e for e in self if e.date.year == year)
      elif month:
         return (e for e in self if e.date.month == month)
      else:
         return self

   @property
   def year(self):
      "iterate over entries from current year alone"
      currentyear = datetime.date.today().year
      return (e for e in self.entries(year=currentyear))


   @property
   def month(self):
      "iterate over entries from current month alone"
      currentmonth = datetime.date.today().month
      return (e for e in self.entries(month=currentmonth))


   def expense(self, year=None, month=None):
      "iterate over expenses, restricted by year or month or both"
      entries = self.entries(year=year, month=month)
      return (e for e in entries if e.amount.is_signed())


   def income(self, year=None, month=None):
      """iterate over income entries restricted by year and/or month, or
         by a list of entry reference numbers"""
      entries = self.entries(year=year, month=month)
      return (e for e in entries if not e.amount.is_signed())


   def referenced(self, references, year=None, month=None):
      """Iterate over income entries restricted by a list of references
         and optionally by year and month. Give None for reference list
         to get entries that have some reference.
      """
      entries = self.entries(year=year, month=month)
      if references:
         return (e for e in entries if e.reference in references)
      else:
         return (e for e in entries if e.reference)

