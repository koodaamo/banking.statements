# -*- encoding: utf-8 -*-
from contextlib import contextmanager
import sys
import string
import codecs
import datetime
import random
import logging
from decimal import Decimal

# wrapped in try..except for use outside
# setuptools environs
try:
   from pkg_resources import iter_entry_points
except:
   pass


logging.basicConfig()
logger = logging.getLogger("banking.statements")


class ColumnMismatchError(Exception):
   "should be raised when a plugin encounters wrong column names"

   def __init__(self, expected, got):
      """sequence of columns that were expected, and another
         consisting of those that were actually read"""

      self.expected = expected
      self.got = got

   def __str__(self):
      expected = '\n '.join( [', '.join(seq) for seq in self.expected] )
      got = ', '.join(self.got)
      return "Expected one of:\n %s\nGot instead:\n %s\n" % (expected, got)



# output utilities

def prettyprint(data):

   print "\n------- Income ---------\n"
   for i in data.income():
      print i.date, i.amount, i.payee_or_recipient, i.description

   print "\n------- Expenses ---------\n"
   for e in data.expense():
      print e.date, e.amount, e.payee_or_recipient, e.description


def get_plugin(bankname):
   "get the plugins by the name given & register the csv dialect"
   for reader_plugin in iter_entry_points(group='banking.statements.readerplugin', name=bankname):
      return reader_plugin.load()


def get_all_plugins():
   plugins = []
   for plugin in iter_entry_points(group='banking.statements.readerplugin'):
      plugins.append(plugin.name)
   return plugins
