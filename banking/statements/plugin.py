"""This module provides a plugin base class all plugins must derive from.

Each plugin must provide an iterator over all account statement entries. Each
entry is a tuple containing the following fields:

date, amount, description, account, reference, message

Dates are to be python dates, amounts are python Decimals and all
other fields are unicode strings.

Description field is a string typically set by the system, something like
'ATM transfer' or 'PAYMENT', whereas the message field should contain
any message that has been attached by the person making a payment.
"""

import csv
from collections import namedtuple


class ReaderPlugin:
   "base reader for reading from line stream"

   def __init__(self, linestream, debug=False):
      "need an open stream. plugin has to close it, too"
      self._linestream = linestream
      self._debug = debug

   @property
   def closed(self):
      return self._linestream.closed

   def __iter__(self):
      raise NotImplementedError

   def can_parse(self, stream):
      "return True if this plugin can parse the stream"
      raise NotImplementedError


class DummyReaderPlugin(ReaderPlugin):
   "dummy plugin for testing etc - just init it with an iterable"

   _closed = False

   @property
   def closed(self):
      return self._closed

   def __iter__(self):
      for e in self._linestream:
         yield e
      self._closed = True


class CSVReaderPlugin(ReaderPlugin):
   "base for reading from a CSV file"

   # set this to the encoding of the CSV file
   ENCODING = None # iso8859-1, 1252, utf-8, whatever


   def __init__(self, linestream, dialect='excel', debug=False):
      ReaderPlugin.__init__(self, linestream, debug)
      self._reader = csv.DictReader(self._linestream, dialect=dialect)

      # we provide a decoded unicode fieldname list
      self.fieldnames = [fn.decode(self.ENCODING) for fn in self._reader.fieldnames]


   def format_record(self, line):
      "implements formatting of entry, returning a namedtuple"
      raise NotImplementedError


   def __iter__(self):
      for entry in self._reader:
         for k in entry:
            try:
               entry[k] = entry[k].decode(self.ENCODING)
            except:
               pass
         yield self.format_record(entry)
      self._linestream.close()

